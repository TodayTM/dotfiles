#!/bin/sh

########################################################################
##  _____         _
## |_   _|__   __| | __ _ _   _
##   | |/ _ \ / _` |/ _` | | | |
##   | | (_) | (_| | (_| | |_| |
##   |_|\___/ \____|\____|\__ |
##                        |___/
########################################################################
## Gitlab: https://gitlab.com/TodayTM
########################################################################
## DiscordTag: Today 刀#0737
########################################################################

#Restart pywal
wal -R

#Reinstall betterdiscordc
betterdiscordctl reinstall

#Apply pywal on discord
./pywal-discord/pywal-discord

#Apply steam-pywal
wal_steam

#Clear terminal
clear

#Show neofetch
neofetch
