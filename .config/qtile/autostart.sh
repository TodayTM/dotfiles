#! /bin/bash

#Picom
picom &

#Emacs
emacsclient -c -a 'emacs'
