########################################################################
##  _____         _
## |_   _|__   __| | __ _ _   _
##   | |/ _ \ / _` |/ _` | | | |
##   | | (_) | (_| | (_| | |_| |
##   |_|\___/ \____|\____|\__ |
##                        |___/
########################################################################
## Gitlab: https://gitlab.com/TodaTM
########################################################################
## DiscordTag: Today 刀#0737
########################################################################

#Color Script
colorscript -r

#Aliases
alias sudo="doas"
alias lt="exa --icons -lah --group-directories-first --tree"
alias l="exa -lah --group-directories-first"
alias ls="exa -lah --group-directories-first"
alias v="nvim"
alias nb="newsboat"
alias top="bpytop"
alias pai="paru -S --noconfirm"
alias pau="paru"
alias fup="paru && ~/.emacs.d/bin/doom sync && ~/.emacs.d/bin/doom upgrade && ~/.emacs.d/bin/doom purge && ~/.emacs.d/bin/doom sync && xmonad --recompile && doas pacman -Rs (pacman -Qtdq) $argv"
alias par="paru -Rns"
alias pao="paru -Rs (pacman -Qtdq) $argv"
alias pac="paru -Sc"
alias gc="git clone"
alias gr="git restore --staged"
alias gs="git status"
alias gp="git push"
alias gt="git push && echo OK! | lolcat && git push lab && echo DONE! | lolcat"
alias gl="git pull"
alias g.="git add ."
alias ga="git add"
alias gm="git commit -m"
alias info="neofetch"
alias cmatrix="unimatrix -l aAcCeGgkmrRsS -f -b"
alias star-wars="telnet towel.blinkenlights.nl"
alias esyn="~/.emacs.d/bin/doom sync"
alias eup="~/.emacs.d/bin/doom upgrade"
alias epur="~/.emacs.d/bin/doom purge"
alias f="lf"
alias vpn="sudo protonvpn connect"
alias vpo="sudo protonvpn disconnect"
alias yay="paru"
alias yaourt="paru"
alias trizen="paru"
alias pikaur="paru"
alias pacman="paru"

#Z.LUA
export _ZL_MATCH_MODE=1
