########################################################################
##  _____         _
## |_   _|__   __| | __ _ _   _
##   | |/ _ \ / _` |/ _` | | | |
##   | | (_) | (_| | (_| | |_| |
##   |_|\___/ \____|\____|\__ |
##                        |___/
########################################################################
## Gitlab: https://gitlab.com/TodaTM
########################################################################
## DiscordTag: Today 刀#0737
########################################################################

function fish_prompt
  set -l last_status $status
  set -g red (set_color -o red)
  set -l green (set_color -o green)

  set -g whitespace ' '

  if test $last_status = 0
    set status_indicator "$green❯"
  else
    set status_indicator "$red✖"
  end

  echo -n -s $status_indicator $whitespace
end
