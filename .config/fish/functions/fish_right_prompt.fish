########################################################################
##  _____         _
## |_   _|__   __| | __ _ _   _
##   | |/ _ \ / _` |/ _` | | | |
##   | | (_) | (_| | (_| | |_| |
##   |_|\___/ \____|\____|\__ |
##                        |___/
########################################################################
## Gitlab: https://gitlab.com/TodaTM
########################################################################
## DiscordTag: Today 刀#0737
########################################################################

function fish_right_prompt

  set -l cyan (set_color -o cyan)
  set -g red (set_color -o red)
  set -g blue (set_color -o blue)
  set -l green (set_color -o green)
  set -l green (set_color -o green)
  set -g normal (set_color normal)
  set -g yellow (set_color yellow)

  set -l ahead (_git_ahead)
  set -g whitespace ' '

  set -l cwd $blue [(basename (prompt_pwd))]

  if [ (_git_branch_name) ]

    if test (_git_branch_name) = 'master'
      set -l git_branch  (_git_branch_name)
      set git_info "$red [$git_branch]"
    else
      set -l git_branch  (_git_branch_name)

      set git_info "$red [$git_branch]"
    end

    if [ (_is_git_dirty) ]
      set -l dirty "$yellow [ ]$cyan"
      set git_info "$git_info$dirty"
    else
      set -l dirty "$cyan"
      set git_info "$git_info$dirty"
    end
  end

  echo -n -s $cwd $git_info $ahead $green $whitespace [(whoami)@(cat /etc/hostname)]
end

function _git_ahead
  set -l commits (command git rev-list --left-right '@{upstream}...HEAD' ^/dev/null)
  if [ $status != 0 ]
    return
  end
  set -l behind (count (for arg in $commits; echo $arg; end | grep '^<'))
  set -l ahead  (count (for arg in $commits; echo $arg; end | grep -v '^<'))
  switch "$ahead $behind"
    case ''     # no upstream
    case '0 0'  # equal to upstream
      return
    case '* 0'  # ahead of upstream
      echo " [$cyan↑$normal_c$ahead$cyan]"
    case '0 *'  # behind upstream
      echo " [$cyan↓$normal_c$behid$cyan]"
    case '*'    # diverged from upstream
      echo " [$cyan↑$normal$ahead$cyan↓$normal_c$behind$cyan]"
  end
end

function _git_branch_name
  echo (command git symbolic-ref HEAD ^/dev/null | sed -e 's|^refs/heads/||')
end

function _is_git_dirty
  echo (command git status -s --ignore-submodules=dirty ^/dev/null)
end
