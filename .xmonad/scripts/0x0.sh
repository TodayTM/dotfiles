#!/usr/bin/sh

# Select File
file=$(exa -a | dmenu -l 10 | cat)

# Upload the file
curl -F "file=@$file" https://0x0.st > $HOME/0x0-last

last=$(cat ~/0x0-last)

#Opens the file in your browser
librewolf $last &
