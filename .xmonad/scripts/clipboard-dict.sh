#!/usr/bin/sh

########################################################################
##  _____         _
## |_   _|__   __| | __ _ _   _
##   | |/ _ \ / _` |/ _` | | | |
##   | | (_) | (_| | (_| | |_| |
##   |_|\___/ \____|\____|\__ |
##                        |___/
########################################################################
## Gitlab: https://gitlab.com/TodaTM
########################################################################
## Element: @today:today-sf.xyz
########################################################################

# Dependencies: Xclip, ST, Bat

xclip -selection c -o | xargs -I {} dict "{}" | cat > ~/dict-temp && st -e bat ~/dict-temp && rm ~/dict-temp
