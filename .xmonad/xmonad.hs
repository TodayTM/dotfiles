------------------------------------------------------------------------
--  _____         _
-- |_   _|__   __| | __ _ _   _
--   | |/ _ \ / _` |/ _` | | | |
--   | | (_) | (_| | (_| | |_| |
--   |_|\___/ \____|\____|\__ |
--                        |___/
------------------------------------------------------------------------
-- Gitlab: https://gitlab.com/TodaTM
------------------------------------------------------------------------
-- Element: @today:today-sf.xyz
------------------------------------------------------------------------

------------------------------------------------------------------------
-- IMPORTS
------------------------------------------------------------------------
import System.IO
import System.Exit
import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers(doFullFloat, doCenterFloat, isFullscreen, isDialog)
import XMonad.Util.Run
import XMonad.Util.EZConfig
import Graphics.X11.ExtraTypes.XF86
import XMonad.Actions.SpawnOn
import XMonad.Actions.WithAll (sinkAll, killAll)
import XMonad.Layout.Spacing
import XMonad.Layout.Fullscreen (fullscreenFull)
import XMonad.Layout.Spiral(spiral)
import XMonad.Layout.ThreeColumns
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.Grid
import XMonad.Layout.ResizableTile
import Data.Monoid
import qualified XMonad.StackSet as W
import qualified Data.Map as M
import qualified Data.ByteString as B
import qualified DBus as D
import qualified DBus.Client as D
import qualified Codec.Binary.UTF8.String as UTF8

------------------------------------------------------------------------
-- GENERAL SETTINGS
------------------------------------------------------------------------
-- TERMINAL
myTerminal      = "st"
-- MOUSE FOCOUS
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False
myClickJustFocuses :: Bool
myClickJustFocuses = False
-- BORDER SIZE
myBorderWidth   = 2
-- MOD KEY (1 = Alt, 4 = Super)
myModMask       = mod4Mask
-- WORKSPACES NAME
myWorkspaces    = ["I","II","III","IV","V","VI","VII","VIII","IX"]
-- BORDER COLORS
myFocusedBorderColor  = "#98BE65"
myNormalBorderColor = "#BBC2CF"

------------------------------------------------------------------------
-- KEY BINDINGS
-----------------------------------------------------------------------
myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
  -- TERMINAL
  [ ((modm,xK_Return), spawn $ XMonad.terminal conf)
  -- GAPS
  , ((modm,xK_o), decWindowSpacing 4)
  , ((modm,xK_p), incWindowSpacing 4)
  , ((modm .|. shiftMask,xK_o), decScreenSpacing 4)
  , ((modm .|. shiftMask,xK_p), incScreenSpacing 4)
  -- DMENU
  , ((modm,xK_d), spawn "dmenu_run")
  -- CLOSE WINDOW
  , ((modm,xK_q), kill)
  -- XKILL
  , ((modm,xK_x), spawn "xkill")
  -- 0x0
  , ((modm,xK_0), spawn "sh $HOME/.xmonad/scripts/0x0.sh")
  -- STRUTS
  , ((modm,xK_f), sendMessage ToggleStruts)
  -- SSH
  , ((modm .|. shiftMask, xK_k), spawn "st -e ssh root@today-sf.xyz")
  -- DICT
  , ((modm,xK_i), spawn "$HOME/.xmonad/scripts/clipboard-dict.sh")
  -- YOUTUBE
  , ((modm .|. shiftMask, xK_y), spawn "$HOME/.xmonad/scripts/yt.sh -r")
  -- LMMS
  , ((modm .|. shiftMask, xK_c), spawn "lmms")
  -- UPDATE
  , ((modm .|. shiftMask, xK_u), spawn "st -e doas pacman -Syu --noconfirm && yay -Syu --aur --noconfirm && ~/.emacs.d/bin/doom sync && ~/.emacs.d/bin/doom upgrade && ~/.emacs.d/bin/doom purge && ~/.emacs.d/bin/doom sync && xmonad --recompile && doas pacman -Rs (pacman -Qtdq) $argv")
  -- LF
  , ((modm .|. shiftMask, xK_a), spawn "st -e lf")
  -- MOC
  , ((modm .|. shiftMask, xK_m), spawn "st -e mocp")
  -- NEWSBOAT
  , ((modm .|. shiftMask, xK_b), spawn "st -e newsboat")
  -- CLOSE ALL WINDOWS
  , ((modm .|. shiftMask,xK_q), killAll)
  -- LIBREWOLF
  ,  ((modm .|. shiftMask, xK_f), spawn "librewolf moz-extension://ed84c35b-ed4c-4dc1-8be6-116a31bc23b5/static/newtab.html")
  -- POLYBAR
  ,  ((modm,xK_y), spawn "polybar-msg cmd toggle")
  -- EMACS
  , ((modm .|. shiftMask, xK_v), spawn "emacsclient -c -a 'emacs'")
  -- LBRY
  , ((modm .|. shiftMask, xK_l), spawn "lbry")
  -- STEAM
  , ((modm .|. shiftMask, xK_s), spawn "steam")
  -- GIMP
  , ((modm .|. shiftMask, xK_i), spawn "gimp")
  -- LUTRIS
  , ((modm .|. shiftMask, xK_g), spawn "lutris")
  -- MULTIMEDIA KEYS
  , ((0, xF86XK_AudioLowerVolume   ), spawn "amixer set Master 10%-")
  , ((0, xF86XK_AudioRaiseVolume   ), spawn "amixer set Master 10%+")
  , ((0, xF86XK_AudioMute          ), spawn "amixer set Master toggle")
  -- ELEMENT
  , ((modm .|. shiftMask, xK_d), spawn "element-desktop-nightly")
  -- SIMPLENOTE
  , ((modm .|. shiftMask, xK_n), spawn "simplenote")
  -- CHANGE LAYOUT
  , ((modm,xK_space), sendMessage NextLayout)
  -- CHANGE FOCOUS
  , ((modm, xK_Tab), windows W.focusDown)
  -- MOVE WINDOW DOWN
  , ((modm .|. shiftMask, xK_j), windows W.swapDown)
  -- SWAP WINDOW UP
  , ((modm .|. shiftMask, xK_k ), windows W.swapUp)
  -- RESIZE (SHRINK HORIZONTAL)
  , ((modm,xK_h), sendMessage Shrink)
  -- RESIZE (EXPAND HORIZONTAL)
  , ((modm,xK_l), sendMessage Expand)
  -- RESIZE (SHRINK		VERTICAL)
  , ((modm,xK_j), sendMessage MirrorShrink)
  -- RESIZE (EXPAND VERTICAL)
  , ((modm,xK_k), sendMessage MirrorExpand)
  --  RESET LAYOUT
  , ((modm .|. shiftMask, xK_space ), setLayout $ XMonad.layoutHook conf)
  -- TILING MODE
  , ((modm,xK_t), withFocused $ windows . W.sink)
  -- QUIT
  , ((modm .|. controlMask, xK_p   ), spawn "st -e doas systemctl poweroff")
  -- RESTART (COMPUTER)
  , ((modm .|. controlMask, xK_s   ), spawn "st -e doas systemctl poweroff")
  -- QUIT
  , ((modm .|. controlMask, xK_q   ), io (exitWith ExitSuccess))
  -- RESTART (WM)
  , ((modm .|. controlMask, xK_r   ), spawn "xmonad --recompile; xmonad --restart")
  -- SCREENSHOT
  ,((0,xK_Print), spawn "flameshot gui")
    ]
  -- WORKSPACE SWITCH
    ++
    [((m .|. modm, k), windows $ f i)
        | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_9]
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++
    -- WINDOW WORKSPACE SWITCH
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f))
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [0..]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]
  -- MOUSE BINDINGS
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $
    -- SET FLOATING
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))
    -- RAISE WINDOW TO THE  TOP OF THE STACK
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))
    
    -- RESIZE
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w >> windows W.shiftMaster))

    
    ]

------------------------------------------------------------------------
-- LAYOUTS
------------------------------------------------------------------------
myLayout = avoidStruts $ spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True $  ResizableTall 1 (3/100) (1/2) [] ||| spiral (6/7) ||| Grid ||| ThreeColMid 1 (3/100) (1/2) ||| Mirror tiled ||| Full
    where
        tiled = Tall nmaster delta tiled_ratio
        nmaster = 1
        delta = 3/100
        tiled_ratio = 1/2

------------------------------------------------------------------------
-- WINDOW RULES
-----------------------------------------------------------------------
myManageHook :: XMonad.Query (Data.Monoid.Endo WindowSet)
myManageHook = composeAll
     [ className =? "LibreWolf" --> doShift ( myWorkspaces !! 0 )
     , className =? "lmms"    --> doShift ( myWorkspaces !! 7)
     , className =? "Steam"   --> doShift ( myWorkspaces !! 3)
     , className =? "Lutris"   --> doShift ( myWorkspaces !! 3)
     , className =? "Element-Nightly" --> doShift ( myWorkspaces !! 2)
     , className =? "spotify" --> doShift ( myWorkspaces !! 4)
     , className =? "Emacs" --> doShift ( myWorkspaces !! 1)
     , (className =? "mpv" --> doFullFloat)
     ]
------------------------------------------------------------------------
-- EVENT HANDLING
------------------------------------------------------------------------
myEventHook = ewmhDesktopsEventHook

------------------------------------------------------------------------
-- AUTO START
------------------------------------------------------------------------
startup :: X ()
startup = do
          spawn "$HOME/.xmonad/scripts/xmonad-start.sh"

------------------------------------------------------------------------
-- MAIN FUCTION
------------------------------------------------------------------------
main :: IO ()
main = do
    dbus <- D.connectSession
    -- REQUEST DBUS NAME
    D.requestName dbus (D.busName_ "org.xmonad.Log")
        [D.nameAllowReplacement, D.nameReplaceExisting, D.nameDoNotQueue]
    xmonad $ewmh $ docks $ defaults { logHook = dynamicLogWithPP (myLogHook dbus) }
myLogHook :: D.Client -> PP
myLogHook dbus = def
    { ppOutput = dbusOutput dbus
    }
-- DBUS SIGNAL LOG UPDATES
dbusOutput :: D.Client -> String -> IO ()
dbusOutput dbus str = do
    let signal = (D.signal objectPath interfaceName memberName) {
            D.signalBody = [D.toVariant $ UTF8.decodeString str]
        }
    D.emit dbus signal
  where
    objectPath = D.objectPath_ "/org/xmonad/Log"
    interfaceName = D.interfaceName_ "org.xmonad.Log"
    memberName = D.memberName_ "Update"
defaults = def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,
        keys               = myKeys,
        mouseBindings      = myMouseBindings,
        layoutHook         = myLayout,
        manageHook         = myManageHook,
        handleEventHook    = myEventHook,
        startupHook        = startup
    }

------------------------------------------------------------------------
-- END
------------------------------------------------------------------------
