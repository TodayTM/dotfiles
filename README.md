# Dotfiles
![Ferrari](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/ferrari.png)
With these configs your computer will run programs as fast as an F40!

## Screenshots:
### Pywal:
![Pywal](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/pywal.png)

### Xmonad:
![Xmonad](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/xmonad.gif)
![Xmonad 02](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/xmonad-02.gif)
![Xmonad 03](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/xmonad-03.gif)

### Alacritty:
![Alacritty](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/alacritty.png)

### Fish:
![Fish](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/fish.png)
![Fish 02](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/fish02.png)
![Fish 03](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/fish03.png)

### Doom Emacs:
![Doom](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/doom-emacs.png)

### Lf:
![Lf](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/lf.png)

### Neofetch:
![Neofetch](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/neofetch.png)

### Polybar:
![Polybar](https://gitlab.com/TodayTM/dotfiles/-/raw/master/screenshots/polybar.png)
